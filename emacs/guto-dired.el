;; (require 'dired-single)

;; (global-set-key (kbd "C-x C-d") 'dired-single-magic-buffer)
;; (global-set-key (kbd "C-x d") 'ido-dired)

;; (defun my-dired-init ()
;;   "Bunch of stuff to run for dired, either immediately or when it's
;;           loaded."
;;   ;; <add other stuff here>
;;   (define-key dired-mode-map [return] 'dired-single-buffer)
;;   (define-key dired-mode-map [mouse-1] 'dired-single-buffer-mouse)
;;   (define-key dired-mode-map "^"
;;     (function
;;      (lambda nil (interactive) (dired-single-buffer "..")))))

;; ;; if dired's already loaded, then the keymap will be bound
;; (if (boundp 'dired-mode-map)
;;     ;; we're good to go; just add our bindings
;;     (my-dired-init)
;;   ;; it's not loaded yet, so add our bindings to the load-hook
;;   (add-hook 'dired-load-hook 'my-dired-init))

;; Open files in dired mode using 'open'
(eval-after-load "dired"
  '(progn
     (define-key dired-mode-map (kbd "z")
       (lambda () (interactive)
         (let ((fn (dired-get-file-for-visit)))
           (start-process "default-app" nil "open" fn))))))
